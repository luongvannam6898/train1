package com.example.train.mapper;

import com.example.train.domain.Item;

import java.util.List;

public interface ItemMapper {
    List<Item> selectAllItem();
    Item selectItemById(Integer id);
}
