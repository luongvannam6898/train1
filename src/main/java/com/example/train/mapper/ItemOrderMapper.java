package com.example.train.mapper;

import com.example.train.domain.ItemOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ItemOrderMapper {
    List<ItemOrder> selectItemOrderByOrderId(Integer orderId);

    Integer insertItemOrder(ItemOrder itemOrder);

    void deleteByOrderId(int orderId);
}
