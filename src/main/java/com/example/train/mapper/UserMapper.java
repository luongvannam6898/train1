package com.example.train.mapper;

import com.example.train.domain.User;

import java.util.List;

public interface UserMapper {
    int insertUser(User user);

    int updateUser(User user);

    User selectUserById(int id);

    List<User> selectAllUser();
    User selectUserByUsername(String username);
}
