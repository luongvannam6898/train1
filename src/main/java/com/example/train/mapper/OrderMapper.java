package com.example.train.mapper;

import com.example.train.domain.Order;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderMapper {
    List<Order>selectAllOrder();

    Integer insertOrder(Order order);
    int deleteById(int id);

}
