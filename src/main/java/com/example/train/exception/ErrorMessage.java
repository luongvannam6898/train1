package com.example.train.exception;

import lombok.Data;

import java.util.Date;

@Data
public class ErrorMessage {
    private String status;
    private String mess;
    private Date date;
    private String decription;

    public ErrorMessage(String status, String mess, Date date, String decription) {
        this.status = status;
        this.mess = mess;
        this.date = date;
        this.decription = decription;
    }
}
