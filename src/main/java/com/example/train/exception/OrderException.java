package com.example.train.exception;

public class OrderException extends RuntimeException{
    public OrderException(String ex){
        super(ex);
    }
}
