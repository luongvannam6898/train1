package com.example.train.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@RestControllerAdvice
public class RestCtrlExceptionHandle {
    @ExceptionHandler(OrderException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorMessage OderNotValid(RuntimeException ex, WebRequest request){
        return new ErrorMessage(
                HttpStatus.BAD_REQUEST.toString(),
                ex.getMessage(),
                new Date(),
                request.getContextPath()
        );

    }
}
