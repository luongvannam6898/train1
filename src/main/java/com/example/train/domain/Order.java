package com.example.train.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Order {
    private Integer id;
    private User user;
    private String oderCode;
    private Date pickupDate;
    private Date deliveryDate;
    private String pickupAddress;
    private String deliveryAddress;
    private List<ItemOrder> itemOrderList = new ArrayList<>();

}
