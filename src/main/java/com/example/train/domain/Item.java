package com.example.train.domain;

import lombok.Data;

@Data
public class Item {
    private Integer id;
    private String name;
}
