package com.example.train.domain;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;
    private String fullname;
    private String phoneNumber;
    private Date dateOfBirth;
    private String username;
    private String password;
}
