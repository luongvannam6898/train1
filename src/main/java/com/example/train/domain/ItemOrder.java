package com.example.train.domain;

import lombok.Data;

@Data
public class ItemOrder {
    private Integer id;
    private Order order;
    private Item item;
    private int quantity;
}
