package com.example.train.dto;

import lombok.Data;

@Data
public class ItemDto {
    private Integer id;
    private String name;
}
