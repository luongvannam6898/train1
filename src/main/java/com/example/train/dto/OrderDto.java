package com.example.train.dto;

import com.example.train.domain.ItemOrder;
import com.example.train.domain.User;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class OrderDto {
    public static AtomicInteger codeAutomatic = new AtomicInteger(1);
    private Integer id;
    private UserDto userDto;
    private String oderCode;
    @NotNull(message = "pickupDate is not blank")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date pickupDate;
    @NotNull(message = "deliveryDate is not blank")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date deliveryDate;
    @NotBlank(message = "pickupAddress is not blank")
    private String pickupAddress;
    @NotBlank(message = "deliveryAddress is not blank")
    private String deliveryAddress;
    private List<ItemOrderDto> itemOrderDtoList;
}
