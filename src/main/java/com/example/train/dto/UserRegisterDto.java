package com.example.train.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
public class UserRegisterDto extends UserDto {

    private String username;
    @NotBlank(message = "password not blank")
    private String password;
}
