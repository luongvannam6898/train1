package com.example.train.dto;

import com.example.train.domain.Item;
import com.example.train.domain.Order;
import lombok.Data;

@Data
public class ItemOrderDto {
    private Integer id;
    private OrderDto orderDto;
    private ItemDto itemDto;
    private int quantity;
}
