package com.example.train.config;

import com.example.train.domain.Order;
import com.example.train.mapper.*;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"com.example.train.mapper"})
public class DbConfigBean {
//    @Bean
//    public SqlSessionFactoryBean sqlSessionFactory() throws Exception {
//        Resource resource = new ClassPathResource("SqlMapConfig.xml");
//        System.out.println("Resouce:"+resource);
//        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
//        sqlSessionFactory.setDataSource(dataSource());
//        sqlSessionFactory.setConfigLocation(resource);
//        return sqlSessionFactory;
//    }
//
//    // scan tất cả những mapper package vn.viettuts.mapper
//    @Bean
//    public MapperScannerConfigurer mapperScannerConfigurer() {
//        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
//        mapperScannerConfigurer.setBasePackage("com.train.springtest.mapper");
//        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
//        return mapperScannerConfigurer;
//    }
//
//    @Bean (name = "userMapper")
//    public UserMapper UserMapper() throws Exception {
//        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
//        return sessionTemplate.getMapper(UserMapper.class);
//    }
//    @Bean (name = "tableCreatorMapper")
//    public TableCreatorMapper tableCreatorMapper() throws Exception {
//        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
//        return sessionTemplate.getMapper(TableCreatorMapper.class);
//    }

    @Bean
    public DriverManagerDataSource dataSource() throws IOException {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        Properties properties = new Properties();
        InputStream user_props = this.getClass()
                .getResourceAsStream("/application.properties");
        properties.load(user_props);
        dataSource.setDriverClassName(
                properties.getProperty("spring.datasource.driverClassName"));
        dataSource.setUrl(properties.getProperty("spring.datasource.url"));
        dataSource.setUsername(
                properties.getProperty("spring.datasource.username"));
        dataSource.setPassword(
                properties.getProperty("spring.datasource.password"));
        return dataSource;

    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactory() throws Exception {
        Resource resource = new ClassPathResource("SqlMapConfig1.xml");
        System.out.println("Resouce:"+resource);
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource());
        sqlSessionFactory.setConfigLocation(resource);
        return sqlSessionFactory;
    }

    // scan tất cả những mapper package vn.viettuts.mapper
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.example.train.mapper");
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        return mapperScannerConfigurer;
    }

    @Bean (name = "userMapper")
    public UserMapper UserMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
        return sessionTemplate.getMapper(UserMapper.class);
    }

    @Bean (name = "itemMapper")
    public ItemMapper itemMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
        return sessionTemplate.getMapper(ItemMapper.class);
    }

    @Bean (name = "orderMapper")
    public OrderMapper orderMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
        return sessionTemplate.getMapper(OrderMapper.class);
    }

    @Bean (name = "itemOrderMapper")
    public ItemOrderMapper itemOrderMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
        return sessionTemplate.getMapper(ItemOrderMapper.class);
    }
    @Bean (name = "tableCreatorMapper")
    public TableCreatorMapper tableCreatorMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(Objects.requireNonNull(sqlSessionFactory().getObject()));
        return sessionTemplate.getMapper(TableCreatorMapper.class);
    }
}
