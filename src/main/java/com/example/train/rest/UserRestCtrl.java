package com.example.train.rest;

import com.example.train.dto.UserDto;
import com.example.train.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserRestCtrl {

    @Autowired
    private UserService userService;
    @PostMapping(value = "/")
    public ResponseEntity<?> saveUser(@RequestBody UserDto userDto) throws JsonProcessingException {
        return new ResponseEntity<>(userService.save(userDto), HttpStatus.OK);
    }

    @GetMapping("/get-all")
    public ResponseEntity<?> getAll() throws JsonProcessingException {
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }
    @GetMapping("/create-table")
    public String createTable(){
        userService.createTable();
        return "OK";
    }
}
