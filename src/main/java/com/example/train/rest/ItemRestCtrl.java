package com.example.train.rest;

import com.example.train.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/item")
public class ItemRestCtrl {
    @Autowired
    private ItemService itemService;
    @GetMapping("get-all")
    public ResponseEntity<?> getAll(){
        return new ResponseEntity<>(itemService.getAll(), HttpStatus.OK);
    }
}
