package com.example.train.rest;

import com.example.train.dto.UserDto;
import com.example.train.dto.UserRegisterDto;
import com.example.train.service.RabitMQProducer;
import com.example.train.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthRestCtrl {
    @Autowired
    private UserService userService;

    @Autowired
    private RabitMQProducer rabitMQProducer;
    @PostMapping("/login")
    public String generateToken(@RequestBody UserRegisterDto request){
        return userService.generateTokenLogin(request);
    }

    @PostMapping("/create-user-login")
    public ResponseEntity<?> saveUser(@RequestBody UserRegisterDto request) throws JsonProcessingException {
        return new ResponseEntity<>(userService.saveUserLogin(request), HttpStatus.OK);
    }
    @GetMapping("/test-rabbit-mp")
    public String getString(){
        rabitMQProducer.sendMessage("Hello");
        return "OK";
    }
}
