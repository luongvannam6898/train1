package com.example.train.rest;

import com.example.train.domain.Order;
import com.example.train.dto.OrderDto;
import com.example.train.service.OderSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/order")
public class OderRestCtrl {
    @Autowired
    private OderSevice oderSevice;
    @GetMapping("get-all")
    public ResponseEntity<?> getAll(){
        System.out.println(oderSevice.getAll());
        return new ResponseEntity<>(oderSevice.getAll(), HttpStatus.OK);
    }

    @PostMapping
    public int save(@RequestBody @Valid OrderDto order){

        return oderSevice.save(order);
    }
    @DeleteMapping("/{id}")
    public int delete(@PathVariable Integer id){
        oderSevice.delete(id);
        return oderSevice.delete(id);
    }
}
