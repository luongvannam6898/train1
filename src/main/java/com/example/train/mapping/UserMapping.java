package com.example.train.mapping;

import com.example.train.domain.User;
import com.example.train.dto.UserDto;
import com.example.train.dto.UserRegisterDto;

public interface UserMapping {
    User dtoToDomain(UserDto userDto, User user);

    UserDto domainToDto(User user, UserDto userDto);
    User userRefisterToDomain(UserRegisterDto userRegister, User user);
}
