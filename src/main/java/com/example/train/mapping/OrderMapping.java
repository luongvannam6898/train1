package com.example.train.mapping;

import com.example.train.domain.Order;
import com.example.train.dto.OrderDto;
import org.springframework.beans.BeanUtils;

public interface OrderMapping {
    Order dtoToDomain(OrderDto dto, Order order);
    Order dtoToDoaminNonItem(OrderDto dto, Order order);
    OrderDto domainToDto(Order order, OrderDto dto);
}
