package com.example.train.mapping;

import com.example.train.domain.ItemOrder;
import com.example.train.dto.ItemOrderDto;

public interface ItemOrderMapping {
    ItemOrder dtoToDomain(ItemOrderDto dto, ItemOrder itemOrder);
    ItemOrder dtoToDomainNonOrder(ItemOrderDto dto, ItemOrder itemOrder);
    ItemOrderDto doaminToDto(ItemOrder domain, ItemOrderDto dto);
}
