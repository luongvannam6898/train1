package com.example.train.mapping.Impl;

import com.example.train.domain.ItemOrder;
import com.example.train.domain.Order;
import com.example.train.dto.ItemOrderDto;
import com.example.train.dto.OrderDto;
import com.example.train.dto.UserDto;
import com.example.train.mapping.ItemOrderMapping;
import com.example.train.mapping.OrderMapping;
import com.example.train.mapping.UserMapping;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class OrderMappingImpl implements OrderMapping {
    @Autowired
    private ItemOrderMapping itemOrderMapping;

    @Autowired
    private UserMapping userMapping;


    @Override
    public Order dtoToDomain(OrderDto dto, Order order) {
        if (dto == null)
            return null;
        setPropertyToDomain(dto, order);
        if(dto.getItemOrderDtoList() != null)
            order.setItemOrderList(dto.getItemOrderDtoList().stream().map(e -> itemOrderMapping.dtoToDomain(e, new ItemOrder())).collect(Collectors.toList()));
        order.setUser(userMapping.dtoToDomain(dto.getUserDto(), order.getUser()));
        return order;
    }

    @Override
    public Order dtoToDoaminNonItem(OrderDto dto, Order order) {
        if (dto == null)
            return null;
        setPropertyToDomain(dto, order);
        return order;
    }

    @Override
    public OrderDto domainToDto(Order order, OrderDto dto) {
        if (order == null)
            return null;
        if (order == null)
            order = new Order();
        BeanUtils.copyProperties(order, dto,"itemOrderList");
        if(order.getItemOrderList() != null)
           dto.setItemOrderDtoList(order.getItemOrderList().stream().map(e->itemOrderMapping.doaminToDto(e, new ItemOrderDto())).collect(Collectors.toList()));
        dto.setUserDto(userMapping.domainToDto(order.getUser(), new UserDto()));
        return dto;
    }

    private void setPropertyToDomain(OrderDto dto, Order order) {
        if (order == null)
            order = new Order();
        BeanUtils.copyProperties(dto, order, "id","itemOrderDtoList");
    }
}
