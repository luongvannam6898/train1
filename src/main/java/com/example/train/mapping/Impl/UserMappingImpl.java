package com.example.train.mapping.Impl;

import com.example.train.domain.User;
import com.example.train.dto.UserDto;
import com.example.train.dto.UserRegisterDto;
import com.example.train.mapping.UserMapping;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserMappingImpl implements UserMapping {
    public User dtoToDomain(UserDto userDto, User user) {
        if (userDto == null)
            return null;
        if (Objects.isNull(user)) {
            user = new User();
        }
        if (Objects.nonNull(user.getId())) {
            userDto.setId(user.getId());
        }
        BeanUtils.copyProperties(userDto, user);
        return user;
    }

    public UserDto domainToDto(User user, UserDto userDto) {
        if (user == null)
            return null;
        if (userDto == null) {
            userDto = new UserDto();
        }
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }

    @Override
    public User userRefisterToDomain(UserRegisterDto userRegister, User user) {
        if (userRegister == null)
            return null;
        if (Objects.isNull(user)) {
            user = new User();
        }
        if (Objects.nonNull(user.getId())) {
            userRegister.setId(user.getId());
        }
        BeanUtils.copyProperties(userRegister, user);
        return user;
    }
}
