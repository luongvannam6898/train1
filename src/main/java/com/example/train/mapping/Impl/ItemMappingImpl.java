package com.example.train.mapping.Impl;

import com.example.train.domain.Item;
import com.example.train.dto.ItemDto;
import com.example.train.mapper.ItemMapper;
import com.example.train.mapping.ItemMapping;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ItemMappingImpl implements ItemMapping {
    @Override
    public ItemDto itemDomainToDto(Item item, ItemDto itemDto) {
        if(item == null)
            return null;
        if(itemDto == null)
            itemDto = new ItemDto();
        BeanUtils.copyProperties(item, itemDto);
        return itemDto;
    }

    @Override
    public Item dtoToDomain(ItemDto dto, Item item) {
        if(dto == null)
            return null;
        if(item == null)
            item = new Item();
        BeanUtils.copyProperties(dto, item);
        return item;
    }
}

