package com.example.train.mapping.Impl;

import com.example.train.domain.ItemOrder;
import com.example.train.dto.ItemDto;
import com.example.train.dto.ItemOrderDto;
import com.example.train.mapping.ItemMapping;
import com.example.train.mapping.ItemOrderMapping;
import com.example.train.mapping.OrderMapping;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemOrderMappingImpl implements ItemOrderMapping {
    @Autowired
    private OrderMapping orderMapping;

    @Autowired
    private ItemMapping itemMapping;
    @Override
    public ItemOrder dtoToDomain(ItemOrderDto dto, ItemOrder itemOrder) {
        if(dto == null)
            return null;
        if(itemOrder == null)
            itemOrder = new ItemOrder();
        BeanUtils.copyProperties(dto, itemOrder, "id");
        itemOrder.setOrder(orderMapping.dtoToDoaminNonItem(dto.getOrderDto(),itemOrder.getOrder()));
        itemOrder.setItem(itemMapping.dtoToDomain(dto.getItemDto(), itemOrder.getItem()));
        return itemOrder;
    }

    @Override
    public ItemOrder dtoToDomainNonOrder(ItemOrderDto dto, ItemOrder itemOrder) {
        return null;
    }

    @Override
    public ItemOrderDto doaminToDto(ItemOrder domain, ItemOrderDto dto) {
        if(domain == null)
            return null;
        if(dto == null)
            dto = new ItemOrderDto();
        BeanUtils.copyProperties(domain, dto);
        dto.setItemDto(itemMapping.itemDomainToDto(domain.getItem(), new ItemDto()));
        return dto;
    }
}
