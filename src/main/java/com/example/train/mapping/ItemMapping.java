package com.example.train.mapping;

import com.example.train.domain.Item;
import com.example.train.dto.ItemDto;

public interface ItemMapping {
    ItemDto itemDomainToDto(Item item, ItemDto itemDto);
    Item dtoToDomain(ItemDto dto, Item item);
}
