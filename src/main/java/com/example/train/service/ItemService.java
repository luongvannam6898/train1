package com.example.train.service;

import com.example.train.dto.ItemDto;

import java.util.List;

public interface ItemService {
    List<ItemDto> getAll();
}
