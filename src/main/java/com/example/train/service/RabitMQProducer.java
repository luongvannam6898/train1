package com.example.train.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

public interface RabitMQProducer {

    public void sendMessage(String message);
}
