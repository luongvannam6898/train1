package com.example.train.service;

import com.example.train.domain.User;
import com.example.train.dto.UserDto;
import com.example.train.dto.UserRegisterDto;

import java.util.List;

public interface UserService {
    UserDto save(UserDto user);
    UserDto update(int id, UserDto userDto);
    List<User> getAll();
    User findById(int id);

    void createTable();

    UserDto getCurrenUser();

    User getUserByUsername(String username);

    String generateTokenLogin(UserRegisterDto loginRequest);

    int saveUserLogin(UserRegisterDto request);
}
