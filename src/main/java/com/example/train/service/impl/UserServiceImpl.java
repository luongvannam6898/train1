package com.example.train.service.impl;

import com.example.train.config.sercurity.JwtUntil;
import com.example.train.domain.User;
import com.example.train.dto.UserDto;
import com.example.train.dto.UserRegisterDto;
import com.example.train.mapper.TableCreatorMapper;
import com.example.train.mapper.UserMapper;
import com.example.train.mapping.UserMapping;
import com.example.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserMapping userMapping;

    @Autowired
    private TableCreatorMapper tableCreatorMapper;

    @Autowired
    private  PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUntil jwtUntil;

    @Override
    public UserDto save(UserDto userDto) {
        User user = userMapping.dtoToDomain(userDto, new User());
        int idUsersave = userMapper.insertUser(user);
        User userSave = userMapper.selectUserById(idUsersave);
        return userMapping.domainToDto(user, new UserDto());
    }

    @Override
    public UserDto update(int id,UserDto userDto) {
        User user = userMapper.selectUserById(id);
        user = userMapping.dtoToDomain(userDto, user);
        int idUserUpdate = userMapper.insertUser(user);
        User userUpdate = userMapper.selectUserById(idUserUpdate);
        return userMapping.domainToDto(user, new UserDto());
    }

    @Override
    public List<User> getAll() {
        return userMapper.selectAllUser();
    }

    @Override
    public User findById(int id) {
        return userMapper.selectUserById(id);
    }

    @Override
    public void createTable() {
        tableCreatorMapper.createUserIfNotExists();
    }



    @Override
    public UserDto getCurrenUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        return userMapping.domainToDto(this.getUserByUsername(currentUserName), new UserDto());
    }

    @Override
    public User getUserByUsername(String username) {
        return userMapper.selectUserByUsername(username);
    }

    @Override
    public String generateTokenLogin(UserRegisterDto loginRequest) {
        User user = this.getUserByUsername(loginRequest.getUsername());
        if(passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())){
            return jwtUntil.generateToken(user);
        }
        throw new RuntimeException("username or password incorrect");
    }

    @Override
    public int saveUserLogin(UserRegisterDto request) {
        User user = userMapping.userRefisterToDomain(request, new User());
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        userMapper.insertUser(user);
//        int idUsersave = userMapper.insertUser(user);
//        User userSave = userMapper.selectUserById(idUsersave);
        userMapper.insertUser(user);
        return user.getId();
    }
}
