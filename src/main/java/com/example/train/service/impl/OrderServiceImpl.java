package com.example.train.service.impl;

import com.example.train.domain.Order;
import com.example.train.dto.OrderDto;
import com.example.train.dto.UserDto;
import com.example.train.exception.OrderException;
import com.example.train.mapper.ItemOrderMapper;
import com.example.train.mapper.OrderMapper;
import com.example.train.mapping.ItemMapping;
import com.example.train.mapping.OrderMapping;
import com.example.train.service.OderSevice;
import com.example.train.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderServiceImpl implements OderSevice {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private ItemOrderMapper itemOrderMapper;

    @Autowired
    private UserService userService;
    @Autowired
    private OrderMapping orderMapping;
    @Override
    public List<OrderDto> getAll() {
        List<Order> orderLisrt = orderMapper.selectAllOrder();
        orderLisrt.forEach(e->e.setItemOrderList(itemOrderMapper.selectItemOrderByOrderId(e.getId())));
        return orderLisrt.stream().map(e->orderMapping.domainToDto(e, new OrderDto())).collect(Collectors.toList());
    }
    public Integer save(OrderDto orderDto){
        if(orderDto.getPickupDate().compareTo(orderDto.getDeliveryDate())>0)
            throw new OrderException("PickupDate is bigger than DeliveryDate");
        orderDto.setUserDto(userService.getCurrenUser());
        Order order = orderMapping.dtoToDomain(orderDto, new Order());
        String codeNumber = String.valueOf(OrderDto.codeAutomatic.incrementAndGet());
        String orderCode = "O"+"000000".substring(0, 6-codeNumber.length())+codeNumber;
        order.setOderCode(orderCode);
        orderMapper.insertOrder(order);
        order.getItemOrderList().forEach(e->{
            e.setOrder(order);
            itemOrderMapper.insertItemOrder(e);
        });
        return order.getId();
    }

    @Override
    public Integer delete(int id) {
        itemOrderMapper.deleteByOrderId(id);
        return id;
    }
}
