package com.example.train.service.impl;

import com.example.train.dto.ItemDto;
import com.example.train.mapper.ItemMapper;
import com.example.train.mapping.ItemMapping;
import com.example.train.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemMapping itemMapping;
    @Override
    public List<ItemDto> getAll() {
        return itemMapper.selectAllItem().stream().map(e->itemMapping.itemDomainToDto(e, new ItemDto())).collect(Collectors.toList());
    }
}
