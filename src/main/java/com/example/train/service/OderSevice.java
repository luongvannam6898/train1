package com.example.train.service;

import com.example.train.domain.Order;
import com.example.train.dto.OrderDto;

import java.util.List;

public interface OderSevice {
    List<OrderDto> getAll();
    Integer save(OrderDto order);
    Integer delete(int id);
}
